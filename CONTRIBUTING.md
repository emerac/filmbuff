# How to contribute

First of all, thank you! It takes time and energy to make contributions, so it
is greatly appreciated that you would spend yours helping out here.

## Reporting bugs

Bug reports are very welcome! If you encounter a bug, don't hesitate to
[open a new issue](https://gitlab.com/emerac/filmbuff/-/issues).

In the interest of fixing bugs as soon as possible, try to include in your
report:

* What you were doing when the bug occurred
* What you expected to happen
* What actually happened (the bug)

Bonus points if you include steps on how to reproduce the bug!

## Requesting a feature

At the moment, the best way to request a feature is to
[open a new issue](https://gitlab.com/emerac/filmbuff/-/issues).

## Writing code

Code contributions can be submitted as pull requests that will be reviewed.
Here are a few pull request guidelines:

* Try to keep your changes small and focused.
* Make sure the test coverage does not change.

---
**Thank you!**
