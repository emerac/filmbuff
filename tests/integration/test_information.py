"""Integration tests for 'filmbuff.information'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pytest

from filmbuff import information, interface


@pytest.mark.display
def test_about_open_close(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    teardown_cons,
):
    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    win = information.AboutWindow(app)
    qtbot.addWidget(win)
    with qtbot.waitExposed(win):
        win.show()

    assert win.windowTitle() == "About - filmbuff"
    assert "TMDb" in win.attribution.text()

    # Cleanup.
    win.close()


@pytest.mark.display
def test_help_open_close(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    teardown_cons,
):
    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    win = information.HelpWindow(app)
    qtbot.addWidget(win)
    with qtbot.waitExposed(win):
        win.show()

    assert win.windowTitle() == "Help - filmbuff"
    assert "General information" in win.display.toPlainText()

    # Cleanup.
    win.close()
