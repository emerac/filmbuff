"""Integration tests for 'filmbuff.__main__'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from unittest import mock

import pytest

from filmbuff.__main__ import main


@pytest.mark.display
def test_main():
    with mock.patch("filmbuff.interface.QApplication") as mock_qapp:
        with mock.patch("filmbuff.interface.Interface") as mock_interface:
            main([])
            mock_qapp.assert_called_once()
            mock_interface.assert_called_once()

    with mock.patch("filmbuff.interface.QApplication") as mock_qapp:
        with mock.patch("filmbuff.interface.Interface") as mock_interface:
            main(["-v"])
            mock_qapp.assert_called_once()
            mock_interface.assert_called_once()

    with mock.patch("filmbuff.interface.QApplication") as mock_qapp:
        with mock.patch("filmbuff.interface.Interface") as mock_interface:
            with mock.patch("filmbuff.arguments.create_shortcut") as mock_cs:
                main(["--create-shortcut"])
                mock_cs.assert_called_once()
                mock_qapp.assert_not_called()
                mock_interface.assert_not_called()
