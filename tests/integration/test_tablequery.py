"""Integration tests for 'filmbuff.tablequery'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pytest

from filmbuff import tablequery


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param(
            "term",
            "field 1 LIKE \"%term%\" ESCAPE \'\\\' OR "
            "field 2 LIKE \"%term%\" ESCAPE \'\\\' OR "
            "field 3 LIKE \"%term%\" ESCAPE \'\\\'",
            id="regular",
        ),
        pytest.param(
            "\"Header 1\":\"term\"",
            "field 1 LIKE \"%term%\" ESCAPE \'\\\'",
            id="non-expanding header",
        ),
    ],
)
def test_main(query, headers, result):
    assert tablequery.main(query, headers) == result


def test_main_expanding(expanding_headers):
    query = "\"cast\":\"term\""
    result = (
        "cast_one LIKE \"%term%\" ESCAPE \'\\\' OR "
        "cast_two LIKE \"%term%\" ESCAPE \'\\\' OR "
        "cast_three LIKE \"%term%\" ESCAPE \'\\\'"
    )
    assert tablequery.main(query, expanding_headers) == result
