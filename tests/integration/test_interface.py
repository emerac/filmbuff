"""Integration tests for 'filmbuff.interface'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from pathlib import Path
from unittest import mock

from PyQt5.QtCore import QSettings, Qt
from PyQt5.QtSql import QSqlDatabase, QSqlTableModel
from PyQt5.QtWidgets import QDialog, QMessageBox
import pytest
import send2trash

from filmbuff import interface
from tests import helpers


@pytest.mark.display
def test_virtual_open_close_settings(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    teardown_cons,
):
    assert Path(app.settings.fileName()).exists()
    assert Path(app.settings.fileName()).parts[1] == "tmp"
    assert app.data_path.exists()
    assert app.data_path.parts[1] == "tmp"
    assert app.con_name == "virtual_1"
    assert QSqlDatabase.database(app.con_name).databaseName() == ":memory:"

    # Use a real QSettings to read values from the generated conf file.
    settings = QSettings(app.settings.fileName(), QSettings.NativeFormat)
    assert settings.value("window/geometry") is None
    assert settings.value("header/state") is None
    app.close()
    assert settings.value("window/geometry") is not None
    assert settings.value("header/state") is not None


@pytest.mark.display
def test_physical_open_close_settings(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    recent_file,
    tmp_path,
    teardown_cons,
):
    # Use a separate QSettings to set conf values before app created.
    settings_path = (tmp_path / ".config" / "filmbuff.conf")
    settings = QSettings(str(settings_path), QSettings.NativeFormat)
    settings.setValue("recent_files", [recent_file])

    # Automatically open the most recent file.
    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    assert Path(app.settings.fileName()).exists()
    assert Path(app.settings.fileName()).parts[1] == "tmp"
    assert app.data_path.exists()
    assert app.data_path.parts[1] == "tmp"
    assert app.con_name == "physical_1"
    assert QSqlDatabase.database(app.con_name).databaseName() == (
        str(recent_file)
    )

    assert settings.value("window/geometry") is None
    assert settings.value("header/state") is None
    app.close()
    assert settings.value("window/geometry") is not None
    assert settings.value("header/state") is not None


@pytest.mark.display
def test_ignore_close_event(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    assert app.isVisible()
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_false,
    )
    app.close()
    assert app.isVisible()
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )


@pytest.mark.display
def test_keypress_event(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )

    # Ctrl-F focuses the searchbar.
    app.view.setFocus()
    assert not app.searchbar.hasFocus()
    qtbot.keyClick(app, Qt.Key_F, Qt.ControlModifier)
    assert app.searchbar.hasFocus()

    # Return when there are no records.
    app.view.setFocus()
    assert len(app.view.selectedIndexes()) == 0
    qtbot.keyClick(app, Qt.Key_Return)

    # Cycle through records with the return key.
    helpers.insert_records(app.model, 3)
    # Hide columns so ID column (not selectable) is the first column.
    for i in range(10):
        app.toggle_header_visibility(i)
        assert app.header.isSectionHidden(i)
    app.view.setCurrentIndex(app.model.createIndex(0, 11))
    for i in range(3):
        assert app.view.selectedIndexes()[0].row() == i
        assert app.view.selectedIndexes()[0].column() == 11
        qtbot.keyClick(app.view, Qt.Key_Return)
    assert app.view.selectedIndexes()[0].row() == 0
    assert app.view.selectedIndexes()[0].column() == 11

    # Make sure all hidden columns are made visible again.
    for i in range(10):
        app.toggle_header_visibility(i)
        assert not app.header.isSectionHidden(i)


@pytest.mark.display
def test_resize_searchbar(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    teardown_cons,
):
    assert app.searchbar.minimumWidth() == 570
    app.resize(899, 400)
    assert app.searchbar.minimumWidth() == 0


@pytest.mark.display
@mock.patch("filmbuff.interface.MessageDialog.exec")
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_export_import(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    mock_message_dialog_exec,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    helpers.insert_records(app.model, 3)

    # Attempt to export, but reject export FileDialog.
    mock_file_dialog_exec.return_value = QDialog.Rejected
    app.export_action.triggered.emit()

    mock_file_dialog_exec.assert_called_once()
    mock_message_dialog_exec.assert_not_called()
    mock_file_dialog_exec.reset_mock()
    mock_message_dialog_exec.reset_mock()

    # Attempt to export, but reject export MessageDialog (no overwrite).
    mock_file_dialog_exec.return_value = QDialog.Accepted
    file_path = (mock_make_data_path / "data.tsv")
    original_contents = "test"
    helpers.create_file(file_path, original_contents)
    file_path_without_ext = str(Path(file_path.parent) / file_path.stem)
    mock_file_dialog_selection.return_value = [file_path_without_ext]
    mock_message_dialog_exec.return_value = QMessageBox.No
    app.export_action.triggered.emit()

    mock_file_dialog_exec.assert_called_once()
    mock_message_dialog_exec.assert_called_once()
    assert helpers.get_file_contents(file_path) == original_contents
    mock_file_dialog_exec.reset_mock()
    mock_message_dialog_exec.reset_mock()

    # Successfully export (overwriting an existing file).
    mock_message_dialog_exec.return_value = QMessageBox.Yes
    app.export_action.triggered.emit()

    mock_file_dialog_exec.assert_called_once()
    mock_message_dialog_exec.assert_called_once()
    assert not helpers.get_file_contents(file_path) == original_contents
    mock_file_dialog_exec.reset_mock()
    mock_message_dialog_exec.reset_mock()

    # Attempt to import, but reject import FileDialog.
    mock_file_dialog_exec.return_value = QDialog.Rejected
    app.import_action.triggered.emit()

    mock_file_dialog_exec.assert_called_once()
    mock_file_dialog_exec.reset_mock()

    # Successfully import the recently exported file.
    mock_file_dialog_exec.return_value = QDialog.Accepted
    mock_file_dialog_selection.return_value = [file_path]
    app.import_action.triggered.emit()

    mock_file_dialog_exec.assert_called_once()
    assert app.model.rowCount() == 6


@pytest.mark.display
@mock.patch("filmbuff.interface.MessageDialog.exec")
def test_filter_view(
    mock_message_dialog_exec,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    helpers.insert_records(app.model, 3)

    # Filter to just one record with a special query.
    app.searchbar.setFocus()
    qtbot.keyClicks(app.searchbar, "\"budget\":\"1\"")
    qtbot.keyClick(app.searchbar, Qt.Key_Return)
    assert app.model.rowCount() == 1

    # Reset the filter.
    app.searchbar.clear()
    assert app.model.rowCount() == 3

    # Try an invalid filter.
    app.searchbar.setFocus()
    qtbot.keyClicks(app.searchbar, "\"cast\":")
    qtbot.keyClick(app.searchbar, Qt.Key_Return)
    mock_message_dialog_exec.assert_called_once()
    assert app.model.rowCount() == 3


@pytest.mark.display
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_simple_undo_redo(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )

    assert app.model.rowCount() == 0
    assert not app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Manually insert 3 records.
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app, Qt.Key_Plus, Qt.ControlModifier)

    assert app.model.rowCount() == 3
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Undo manual insertion (3 times).
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app.view, Qt.Key_Z, Qt.ControlModifier)

    assert app.model.rowCount() == 0
    assert not app.undo_action.isEnabled()
    assert app.redo_action.isEnabled()

    # Redo manual insertion (3 times).
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app.view, Qt.Key_Y, Qt.ControlModifier)

    assert app.model.rowCount() == 3
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Simulate a new sheet with 3 records by clearing undo/redo history.
    app.model.undo_stack = []
    app.model.redo_stack = []
    app.toggle_undo_enable()
    app.toggle_redo_enable()

    assert app.model.rowCount() == 3
    assert not app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Manually delete 3 records.
    app.view.setFocus()
    app.view.clearSelection()
    for i in range(3):
        app.view.setCurrentIndex(app.model.createIndex(0, 0))
        qtbot.keyClick(app, Qt.Key_Minus, Qt.ControlModifier)

    assert app.model.rowCount() == 0
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Undo manual deletions (3 times).
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app.view, Qt.Key_Z, Qt.ControlModifier)

    assert app.model.rowCount() == 3
    assert not app.undo_action.isEnabled()
    assert app.redo_action.isEnabled()

    # Redo manual deletions (3 times).
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app.view, Qt.Key_Y, Qt.ControlModifier)

    assert app.model.rowCount() == 0
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()


@pytest.mark.display
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_complex_undo_redo(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )

    # Manually insert 3 records.
    app.view.setFocus()
    for i in range(3):
        qtbot.keyClick(app, Qt.Key_Plus, Qt.ControlModifier)

    assert app.model.rowCount() == 3
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Manually delete the middle record.
    app.view.setFocus()
    app.view.clearSelection()
    app.view.setCurrentIndex(app.model.createIndex(1, 0))
    qtbot.keyClick(app, Qt.Key_Minus, Qt.ControlModifier)

    assert app.model.rowCount() == 2
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Undo the deletion.
    app.view.setFocus()
    qtbot.keyClick(app.view, Qt.Key_Z, Qt.ControlModifier)

    assert app.model.rowCount() == 3
    assert app.undo_action.isEnabled()
    assert app.redo_action.isEnabled()

    # Redo the deletion.
    app.view.setFocus()
    qtbot.keyClick(app.view, Qt.Key_Y, Qt.ControlModifier)

    assert app.model.rowCount() == 2
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()

    # Undo all operations (1 deletion + 3 insertions).
    app.view.setFocus()
    for i in range(4):
        qtbot.keyClick(app.view, Qt.Key_Z, Qt.ControlModifier)

    assert app.model.rowCount() == 0
    assert not app.undo_action.isEnabled()
    assert app.redo_action.isEnabled()

    # Redo all operations (3 insertions + 1 deletion).
    app.view.setFocus()
    for i in range(4):
        qtbot.keyClick(app.view, Qt.Key_Y, Qt.ControlModifier)

    assert app.model.rowCount() == 2
    assert app.undo_action.isEnabled()
    assert not app.redo_action.isEnabled()


@pytest.mark.display
def test_new_sheet(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    # A new sheet cannot be opened if the state is unresolved.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_false,
    )
    assert app.con_name == "virtual_1"
    app.new_action.triggered.emit()
    assert app.con_name == "virtual_1"

    # A new sheet can be opened if the state is resolved.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    assert app.con_name == "virtual_1"
    app.new_action.triggered.emit()
    assert app.con_name == "virtual_2"


@pytest.mark.display
@mock.patch("filmbuff.interface.MessageDialog.exec")
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_save_as_and_save(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    mock_message_dialog_exec,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    assert app.con_name == "virtual_1"

    # Attempt to save, but reject the 'save as' FileDialog.
    mock_file_dialog_exec.return_value = QDialog.Rejected
    app.save_action.triggered.emit()
    assert app.con_name == "virtual_1"

    # Attempt to save and successfully 'save as'.
    mock_file_dialog_exec.return_value = QDialog.Accepted
    db_path = (mock_make_data_path / "test_1")
    mock_file_dialog_selection.return_value = [str(db_path)]
    app.save_action.triggered.emit()
    assert app.con_name == "physical_1"
    assert (mock_make_data_path / "test_1.sqlite").exists()

    # Attempt to 'save as', but reject the MessageDialog (no overwrite).
    db_path = (mock_make_data_path / "test_2.sqlite")
    original_contents = "test"
    helpers.create_file(db_path, original_contents)
    mock_file_dialog_selection.return_value = [str(db_path)]
    mock_message_dialog_exec.return_value = QMessageBox.No
    app.save_as_action.triggered.emit()
    assert app.con_name == "physical_1"
    assert helpers.get_file_contents(db_path) == original_contents

    # Attempt to 'save as', but fail to overwrite (send2trash error).
    mock_message_dialog_exec.return_value = QMessageBox.Yes
    with mock.patch("filmbuff.interface.send2trash.send2trash") as mock_s2t:
        mock_s2t.side_effect = send2trash.TrashPermissionError(db_path)
        app.save_as_action.triggered.emit()
    assert app.con_name == "physical_1"
    assert helpers.get_file_contents(db_path) == original_contents

    # Successfully 'save as' by overwriting an existing file.
    app.save_as_action.triggered.emit()
    assert app.con_name == "physical_2"
    assert not helpers.get_file_contents(db_path) == original_contents

    # Ensure the currently open file cannot be 'save as' overwritten.
    app.save_as_action.triggered.emit()
    assert app.con_name == "physical_2"

    # Perform a regular save.
    app.save_action.triggered.emit()
    assert app.con_name == "physical_2"


@pytest.mark.display
def test_set_optimal_widths(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    teardown_cons,
):
    app.optimal_widths_action.triggered.emit()
    optimal_header_width = app.header.sectionSize(2)
    app.header.resizeSection(2, 400)
    assert app.header.sectionSize(2) == 400
    app.optimal_widths_action.triggered.emit()
    assert app.header.sectionSize(2) == optimal_header_width


@pytest.mark.display
@mock.patch("filmbuff.interface.PreferencesDialog")
def test_set_preferences(
    mock_pref_dialog,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    settings = QSettings(app.settings.fileName(), QSettings.NativeFormat)

    # Not accepting PreferencesDialog does change any values.
    mock_pref_dialog.return_value.exec.return_value = False
    app.preferences_action.triggered.emit()
    assert settings.value("table/scroll_to_inserted") is None
    assert settings.value("statusbar/duration") is None
    assert settings.value("statusbar/visible") is None

    # Accept the PreferencesDialog, but do not change any settings.
    mock_pref_dialog.return_value.exec.return_value = True
    mock_pref_dialog.return_value.scroll_checkbox.isChecked.return_value = True
    mock_pref_dialog.return_value.duration_spinbox.value.return_value = 5
    mock_pref_dialog.return_value.visible_checkbox.isChecked.return_value = (
        True
    )
    app.preferences_action.triggered.emit()
    assert settings.value("table/scroll_to_inserted") == "1"
    assert settings.value("statusbar/duration") == 5000
    assert settings.value("statusbar/visible") == "1"

    # Change all of the settings.
    mock_pref_dialog.return_value.exec.return_value = True
    mock_pref_dialog.return_value.scroll_checkbox.isChecked.return_value = (
        False
    )
    mock_pref_dialog.return_value.duration_spinbox.value.return_value = 1
    mock_pref_dialog.return_value.visible_checkbox.isChecked.return_value = (
        False
    )
    app.preferences_action.triggered.emit()
    assert settings.value("table/scroll_to_inserted") == "0"
    assert settings.value("statusbar/duration") == 1000
    assert settings.value("statusbar/visible") == "0"


@pytest.mark.display
def test_special_settings_usage(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    monkeypatch,
    tmp_path,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    settings_path = (tmp_path / ".config" / "filmbuff.conf")
    settings = QSettings(str(settings_path), QSettings.NativeFormat)
    # Test StatusBar's usage of the duration.
    settings.setValue("statusbar/duration", "1")
    # Test load_setting's usage of the visible settings.
    settings.setValue("statusbar/visible", "0")

    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    # Test insert_row's usage of the scroll_to_inserted setting.
    settings.setValue("table/scroll_to_inserted", "0")
    app.add_action.triggered.emit()


@pytest.mark.display
def test_restore_defaults(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    teardown_cons,
):
    # Collect the default window geometry values.
    app.restore_window_action.triggered.emit()
    orig_width = app.width()
    orig_height = app.height()
    # Change the window geometry and then restore it to default.
    app.resize(400, 300)
    assert not app.width() == orig_width
    assert not app.height() == orig_height
    app.restore_window_action.triggered.emit()
    assert app.width() == orig_width
    assert app.height() == orig_height

    # Collect the default header state values.
    app.restore_header_action.triggered.emit()
    orig_name = app.model.headerData(3, Qt.Horizontal)
    # Change the header state and then restore it to default.
    app.view.hideColumn(3)
    assert app.view.isColumnHidden(3) is True
    app.header.moveSection(3, 6)
    vis_index = app.header.visualIndex(3)
    assert not app.model.headerData(vis_index, Qt.Horizontal) == orig_name

    # Ensure header check states are saved.
    app.view.hideColumn(3)
    app.close()
    with qtbot.waitExposed(app):
        app.show()
    assert app.header.isSectionHidden(3)


@pytest.mark.display
def test_preferences_dialog(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    teardown_cons,
):
    # Test the default values.
    dialog = interface.PreferencesDialog(app)
    with qtbot.waitExposed(dialog):
        dialog.show()
    assert app.settings.value("table/scroll_to_inserted") is None
    assert app.settings.value("statusbar/duration") is None
    assert app.settings.value("statusbar/visible") is None
    assert dialog.scroll_checkbox.isChecked() is True
    assert dialog.duration_spinbox.value() == 5
    assert dialog.visible_checkbox.isChecked() is True
    dialog.close()

    # Test the changed settings.
    app.settings.setValue("table/scroll_to_inserted", "0")
    app.settings.setValue("statusbar/duration", "1000")
    app.settings.setValue("statusbar/visible", "0")
    dialog = interface.PreferencesDialog(app)
    with qtbot.waitExposed(dialog):
        dialog.show()
    assert dialog.scroll_checkbox.isChecked() is False
    assert dialog.duration_spinbox.value() == 1
    assert dialog.visible_checkbox.isChecked() is False
    dialog.close()


@pytest.mark.display
@mock.patch("filmbuff.interface.Interface.save_model")
@mock.patch("filmbuff.interface.Interface.save_as")
@mock.patch("filmbuff.interface.MessageDialog.exec")
def test_is_state_resolved(
    mock_message_dialog_exec,
    mock_save_as,
    mock_save_model,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    # Close in-memory sheet with nothing to save.
    monkeypatch.setattr(QSqlTableModel, "isDirty", helpers.return_false)
    app.close()
    assert not app.isVisible()

    # Close in-memory sheet with something to save (choose to discard).
    with qtbot.waitExposed(app):
        app.show()
    monkeypatch.setattr(QSqlTableModel, "isDirty", helpers.return_true)
    mock_message_dialog_exec.return_value = QMessageBox.Discard
    app.close()
    assert not app.isVisible()

    # Close in-memory sheet with something to save (choose to save).
    with qtbot.waitExposed(app):
        app.show()
    mock_message_dialog_exec.return_value = QMessageBox.Save
    app.close()
    assert not app.isVisible()

    # Close a physical sheet with something to save (choose to save).
    with qtbot.waitExposed(app):
        app.show()
    app.con_name = "physical_1"
    mock_message_dialog_exec.return_value = QMessageBox.Save
    app.close()
    assert not app.isVisible()

    # Close in-memory sheet with something to save (choose to cancel).
    with qtbot.waitExposed(app):
        app.show()
    mock_message_dialog_exec.return_value = QMessageBox.Cancel
    app.close()
    assert app.isVisible()

    # Make sure the app closes without checking the state.
    with qtbot.waitExposed(app):
        app.show()
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    app.close()
    assert not app.isVisible()


@pytest.mark.display
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_open_and_new(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    assert app.con_name == "virtual_1"

    # Attempt to open an existing sheet when the state is not resolved.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_false,
    )
    app.open_action.triggered.emit()
    assert app.con_name == "virtual_1"

    # Attempt to open an existing sheet, but do not accept the dialog.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    mock_file_dialog_exec.return_value = QDialog.Rejected
    app.open_action.triggered.emit()
    assert app.con_name == "virtual_1"

    # Create an existing sheet and successfully open it.
    mock_file_dialog_exec.return_value = QDialog.Accepted
    db_path = (mock_make_data_path / "test.sqlite")
    db_path.touch()
    mock_file_dialog_selection.return_value = [str(db_path)]
    app.open_action.triggered.emit()
    assert app.con_name == "physical_1"

    # Open a new in-memory sheet.
    app.new_action.triggered.emit()
    assert app.con_name == "virtual_2"


@pytest.mark.display
@mock.patch("filmbuff.interface.Interface")
@mock.patch("filmbuff.interface.QApplication")
def test_main(
    mock_qapp,
    mock_interface,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    teardown_cons,
):
    assert interface.main() is None


@pytest.mark.display
@mock.patch("filmbuff.interface.MessageDialog.exec")
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_recent_files(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    mock_message_dialog_exec,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    assert app.con_name == "virtual_1"

    # Create some files to fill out the recent files list.
    recent_files = []
    for i in range(6):
        file_path = (mock_make_data_path / f"file_{i}.sqlite")
        recent_files.append(file_path)
    app.settings.setValue("recent_files", recent_files)

    # Open a new sheet to trigger a recent files update.
    app.new_action.triggered.emit()
    assert app.con_name == "virtual_2"

    # Attempt to open a recent file if the state is not resolved.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_false,
    )
    app.recent_files_actions[0].triggered.emit()
    assert app.con_name == "virtual_2"

    # Attempt to open a recent file that does not exist.
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    app.recent_files_actions[0].triggered.emit()
    assert app.con_name == "virtual_2"

    # Create a physical database that matches the path of a recent file.
    recent_filename = app.recent_files_actions[0].text()
    db_path = (mock_make_data_path / recent_filename)
    db_path.touch()
    # Successfully open an existing recent file.
    mock_file_dialog_exec.return_value = QDialog.Accepted
    mock_file_dialog_selection.return_value = [str(db_path)]
    app.recent_files_actions[0].triggered.emit()
    assert app.con_name == "physical_1"


@pytest.mark.display
@mock.patch("filmbuff.interface.FileDialog.selectedFiles")
@mock.patch("filmbuff.interface.FileDialog.exec")
def test_recent_files_special_cases(
    mock_file_dialog_exec,
    mock_file_dialog_selection,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    # Open a new sheet to force the recent files (None) to update.
    assert app.settings.value("recent_files") is None
    app.new_action.triggered.emit()

    # Create some files to fill out the recent files list.
    recent_files = []
    for i in range(6):
        file_path = (mock_make_data_path / f"file_{i}.sqlite")
        file_path.touch()
        recent_files.append(file_path)
    app.settings.setValue("recent_files", recent_files)
    app.new_action.triggered.emit()
    assert len(app.settings.value("recent_files")) == 6

    # Open the oldest recent file to put it at the top of the list.
    mock_file_dialog_exec.return_value = QDialog.Accepted
    oldest_file = app.settings.value("recent_files")[0]
    mock_file_dialog_selection.return_value = [str(oldest_file)]
    app.open_action.triggered.emit()
    assert app.settings.value("recent_files")[-1] == oldest_file

    # Open a file not on the recent files list to pop the oldest entry.
    app.settings.setValue("recent_files", recent_files)
    new_file_path = (mock_make_data_path / "test.sqlite")
    new_file_path.touch()
    mock_file_dialog_selection.return_value = [str(new_file_path)]
    assert len(app.settings.value("recent_files")) == 6
    app.open_action.triggered.emit()
    assert len(app.settings.value("recent_files")) == 6


@pytest.mark.display
def test_multi_deletion(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )
    # Attempt to delete rows when nothing is selected.
    app.view.clearSelection()
    app.remove_action.triggered.emit()

    # Delete all five rows with one contiguous selection.
    helpers.insert_records(app.model, 5)
    app.view.clearSelection()
    assert app.model.rowCount() == 5
    app.view.selectColumn(0)
    app.remove_action.triggered.emit()
    assert app.model.rowCount() == 0

    # Delete rows two and five with one non-contiguous selection.
    helpers.insert_records(app.model, 5)
    app.view.clearSelection()
    assert app.model.rowCount() == 5
    app.view.setFocus()
    app.view.selectRow(4)
    qtbot.keyClick(app, Qt.Key_Return, Qt.ControlModifier)
    app.remove_action.triggered.emit()
    assert app.model.rowCount() == 3


@pytest.mark.display
@mock.patch("filmbuff.metadata.MetadataWindow")
@mock.patch("filmbuff.information.HelpWindow")
@mock.patch("filmbuff.information.AboutWindow")
def test_windows(
    mock_about_win,
    mock_help_win,
    mock_metadata_win,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    app,
    monkeypatch,
    teardown_cons,
):
    # Open the 'about' window for the first time.
    assert app.about_window is None
    app.about_action.triggered.emit()

    # Open the 'about' window again (reuse the original ref).
    assert app.about_window is not None
    app.about_action.triggered.emit()

    # Open the 'help' window for the first time.
    assert app.help_window is None
    app.help_action.triggered.emit()

    # Open the 'help' window again (reuse the original ref).
    assert app.help_window is not None
    app.help_action.triggered.emit()

    # Open the metadata window for the first time.
    assert app.metadata_window is None
    app.metadata_action.triggered.emit()

    # Open the metadata window again (reuse the original ref).
    assert app.metadata_window is not None
    app.metadata_action.triggered.emit()
