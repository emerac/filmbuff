"""Integration tests for 'filmbuff.metadata'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from unittest import mock

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTreeWidgetItem
import pytest
import requests

from filmbuff import extdata, interface, metadata
from tests import data, helpers


@pytest.mark.display
def test_open_close(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    teardown_cons,
):
    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    win = metadata.MetadataWindow(app)
    qtbot.addWidget(win)
    with qtbot.waitExposed(win):
        win.show()

    # Simulate a filled out window.
    win.query_field.setText("query")
    win.year_field.setText("1900")
    win.key_field.setText("key")
    item = ["123", "1900", "title", "lead", "director"]
    tree_item = QTreeWidgetItem(item)
    win.tree.addTopLevelItem(tree_item)
    win.next_page = 2
    assert win.query_field.text() == "query"
    assert win.year_field.text() == "1900"
    assert win.key_field.text() == "key"
    assert win.tree.topLevelItemCount() == 1
    assert win.next_page == 2

    # Ensure all fields and values are reset on window close.
    win.close()
    with qtbot.waitExposed(win):
        win.show()
    assert win.query_field.text() == ""
    assert win.year_field.text() == ""
    assert win.key_field.text() == ""
    assert win.tree.topLevelItemCount() == 0
    assert win.next_page == 1

    # Ensure the key is not cleared if 'remember' is checked.
    win.key_field.setText("key")
    win.remember_box.setCheckState(Qt.Checked)
    assert win.key_field.text() == "key"
    assert win.remember_box.isChecked()
    win.close()
    with qtbot.waitExposed(win):
        win.show()
    assert win.key_field.text() == "key"
    assert win.remember_box.isChecked()

    # Cleanup.
    win.close()


@pytest.mark.display
def test_add_item(
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    monkeypatch,
    teardown_cons,
):
    def get_film_info(*args, **kwargs):
        return data.SAMPLE_INFO

    monkeypatch.setattr(extdata, "get_film_info", get_film_info)
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )

    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    win = metadata.MetadataWindow(app)
    qtbot.addWidget(win)
    with qtbot.waitExposed(win):
        win.show()

    # Add an item to the tree.
    item_data = ["1", "1900", "title", "lead", "director"]
    tree_item = QTreeWidgetItem(item_data)
    win.tree.addTopLevelItem(tree_item)

    # Select the tree item and add it to the model.
    assert not win.add_button.isEnabled()
    win.tree.setCurrentItem(tree_item)
    assert win.add_button.isEnabled()
    assert app.model.rowCount() == 0
    win.add_button.pressed.emit()
    assert app.model.rowCount() == 1
    new_record = app.model.record(0)
    assert new_record.value(1) == "lead"

    # Cleanup.
    win.tree.clearSelection()
    win.close()


@pytest.mark.display
@mock.patch("filmbuff.metadata.UnauthorizedErrorDialog")
@mock.patch("filmbuff.metadata.EmptyErrorDialog")
def test_search(
    mock_empty_dialog,
    mock_HTTPError_dialog,
    qtbot,
    mock_qsettings,
    mock_make_data_path,
    monkeypatch,
    teardown_cons,
):
    def raise_HTTPError(*args, **kwargs):
        raise requests.exceptions.HTTPError

    def return_search_results_1(*args, **kwargs):
        return data.SAMPLE_SEARCH_RESULTS_PAGE_1

    def return_search_results_2(*args, **kwargs):
        return data.SAMPLE_SEARCH_RESULTS_PAGE_2

    def get_film_info(*args, **kwargs):
        return data.SAMPLE_INFO

    monkeypatch.setattr(extdata, "get_film_info", get_film_info)
    monkeypatch.setattr(
        interface.Interface,
        "is_state_resolved",
        helpers.return_true,
    )

    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()

    win = metadata.MetadataWindow(app)
    qtbot.addWidget(win)
    with qtbot.waitExposed(win):
        win.show()

    # Searching with an empty query field opens an error dialog.
    win.search_button.pressed.emit()
    mock_empty_dialog.assert_called_once()
    mock_empty_dialog.reset_mock()

    # Searching with an empty key field opens an error dialog.
    win.query_field.setText("query")
    win.search_button.pressed.emit()
    mock_empty_dialog.assert_called_once()
    mock_empty_dialog.reset_mock()

    # Ensure an HTTPError causes an UnauthorizedErrorDialog to open.
    monkeypatch.setattr(extdata, "get_search_results", raise_HTTPError)
    win.query_field.setText("query")
    win.key_field.setText("key")
    win.search_button.pressed.emit()
    mock_empty_dialog.not_called()
    mock_HTTPError_dialog.assert_called_once()
    mock_HTTPError_dialog.reset_mock()

    # Searching with all required fields occupied returns data.
    monkeypatch.setattr(extdata, "get_search_results", return_search_results_1)
    assert win.tree.topLevelItemCount() == 0
    assert not win.load_button.isEnabled()
    win.search_button.pressed.emit()
    mock_empty_dialog.not_called()
    mock_HTTPError_dialog.not_called()
    assert win.tree.topLevelItemCount() == 3
    assert win.load_button.isEnabled()

    # Pressing 'Load' will get the remaining (2) search results.
    monkeypatch.setattr(extdata, "get_search_results", return_search_results_2)
    win.load_button.pressed.emit()
    mock_empty_dialog.not_called()
    mock_HTTPError_dialog.not_called()
    assert win.tree.topLevelItemCount() == 5
    assert not win.load_button.isEnabled()
