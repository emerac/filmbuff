"""Provide fixtures for usage across all tests.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import json
from unittest import mock

from PyQt5.QtSql import (
    QSqlDatabase,
)
import pytest

from filmbuff import interface
from tests import helpers


@pytest.fixture
def app(qtbot):
    app = interface.Interface()
    qtbot.addWidget(app)
    with qtbot.waitExposed(app):
        app.show()
    return app


@pytest.fixture
def data_path(tmp_path):
    data_path = (tmp_path / ".local" / "share" / "filmbuff")
    data_path.mkdir(parents=True)
    return data_path


@pytest.fixture
def expanding_headers():
    return {
        "Cast one": "cast_one",
        "Cast two": "cast_two",
        "Cast three": "cast_three",
        "Genre one": "genre_one",
        "Genre two": "genre_two",
        "Genre three": "genre_three",
    }


@pytest.fixture
def headers():
    return {
        "Header 1": "field 1",
        "Header 2": "field 2",
        "Header 3": "field 3",
    }


@pytest.fixture
@pytest.mark.display
@mock.patch("filmbuff.interface.Interface.make_data_path")
def metawindow_creation(
    mock_data_path,
    data_path,
    monkeypatch,
    tmp_path,
    qtbot,
    teardown_cons,
):
    def return_mock_qsettings(*args, **kwargs):
        return helpers.MockQSettings(tmp_path)

    monkeypatch.setattr(interface, "QSettings", return_mock_qsettings)
    mock_data_path.return_value = data_path


@pytest.fixture
def mock_make_data_path(monkeypatch, tmp_path, data_path):
    def return_mock_data_path(*args, **kwargs):
        return data_path

    monkeypatch.setattr(
        interface.Interface,
        "make_data_path",
        return_mock_data_path,
    )
    return data_path


@pytest.fixture
def mock_qsettings(monkeypatch, tmp_path):
    def return_mock_qsettings(*args, **kwargs):
        return helpers.MockQSettings(tmp_path)

    monkeypatch.setattr(interface, "QSettings", return_mock_qsettings)


@pytest.fixture
def sample_data():
    return json.loads(json.dumps(
        {"credits":
            {"cast": [{"name": "Full Name 1",
                       "order": 0},
                      {"name": "Full Name 2",
                       "order": 1},
                      {"name": "Full Name 3",
                       "order": 2},
                      {"name": "Full Name 4",
                       "order": 3}],
             "crew": [{"name": "Full Name 4",
                       "job": "Director"}]},
         "genres": [{"name": "Genre 1"},
                    {"name": "Genre 2"},
                    {"name": "Genre 3"},
                    {"name": "Genre 4"}]}
        ))


@pytest.fixture
def recent_file(tmp_path):
    path = (tmp_path / ".local" / "share" / "filmbuff" / "test.sqlite")
    path.touch()
    return path


@pytest.fixture
def teardown_cons():
    yield
    for con_name in QSqlDatabase.database().connectionNames():
        con = QSqlDatabase.database(con_name)
        con.close()
        assert not con.isOpen()
        del con
        QSqlDatabase.removeDatabase(con_name)
    assert len(QSqlDatabase.database().connectionNames()) == 0
