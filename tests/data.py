"""Sample data for use in tests.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
SAMPLE_INFO = {
  "budget": 0,
  "genres": [
    {
      "id": 0,
      "name": "Drama"
    },
    {
      "id": 0,
      "name": "History"
    },
    {
      "id": 0,
      "name": "War"
    }
  ],
  "id": 1,
  "imdb_id": "tt0000001",
  "original_language": "de",
  "original_title": "original title",
  "overview": "overview",
  "popularity": 0.000,
  "production_countries": [
    {
      "iso_3166_1": "DE",
      "name": "Germany"
    }
  ],
  "release_date": "1900-01-01",
  "revenue": 0,
  "runtime": 100,
  "spoken_languages": [
    {
      "english_name": "German",
      "iso_639_1": "de",
      "name": "Deutsch"
    },
    {
      "english_name": "English",
      "iso_639_1": "en",
      "name": "English"
    },
    {
      "english_name": "French",
      "iso_639_1": "fr",
      "name": "Fran\u00e7ais"
    }
  ],
  "status": "Released",
  "tagline": "tagline",
  "title": "title",
  "vote_average": 0.0,
  "vote_count": 0,
  "credits": {
    "cast": [
      {
        "name": "lead",
        "order": 1
      },
      {
        "name": "cast two",
        "order": 2
      },
      {
        "name": "cast three",
        "order": 3
      },
      {
        "name": "cast four",
        "order": 4
      }
    ],
    "crew": [
      {
        "name": "director",
        "job": "Director"
      }
    ]
  }
}


SAMPLE_SEARCH_RESULTS_PAGE_1 = {
    "page": 1,
    "results": [
        {
            "id": 1,
            "release_date": "1900-01-01",
            "title": "title",
        },
        {
            "id": 1,
            "release_date": "1900-01-01",
            "title": "title",
        },
        {
            "id": 1,
            "release_date": "1900-01-01",
            "title": "title",
        }
    ],
    "total_pages": 2,
    "total_results": 5
}


SAMPLE_SEARCH_RESULTS_PAGE_2 = {
    "page": 2,
    "results": [
        {
            "id": 1,
            "release_date": "1900-01-01",
            "title": "title",
        },
        {
            "id": 1,
            "release_date": "1900-01-01",
            "title": "title",
        },
    ],
    "total_pages": 2,
    "total_results": 5
}
