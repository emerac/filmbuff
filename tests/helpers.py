"""Provide helper functions for use in tests.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from pathlib import Path
from typing import Any, Dict, Union

from PyQt5.QtCore import QSettings
from PyQt5.QtSql import (
    QSqlDatabase,
    QSqlTableModel,
    QSqlQuery,
)


CREATE_TABLE_SQLSTRING = """
    CREATE TABLE films (
        budget INTEGER,
        cast_one TEXT,
        cast_two TEXT,
        cast_three TEXT,
        color TEXT,
        content TEXT,
        director TEXT,
        genre_one TEXT,
        genre_two TEXT,
        genre_three TEXT,
        id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE NOT NULL,
        imdb_id TEXT,
        notes TEXT,
        original_language TEXT,
        original_title TEXT,
        overview TEXT,
        popularity REAL,
        production_country TEXT,
        release_date TEXT,
        revenue INTEGER,
        runtime INTEGER,
        spoken_language TEXT,
        status TEXT,
        tagline TEXT,
        title TEXT,
        tmdb_id INTEGER,
        vote_average REAL,
        vote_count INTEGER
    )
"""


class MockQSettings(QSettings):
    def __init__(self, base_path: Path) -> None:
        self.base_path = base_path
        self.settings_path = (self.base_path / ".config" / "filmbuff.conf")
        self.real_settings = QSettings(
            str(self.settings_path),
            QSettings.NativeFormat,
        )
        super().__init__(str(self.settings_path), QSettings.NativeFormat)

    def fileName(self) -> str:
        return str(self.settings_path)

    def value(
        self,
        setting: str,
        defaultValue: Any = None,
        type: Any = None,
    ) -> Any:
        return self.real_settings.value(setting)


def count_rows(con_name: str) -> int:
    query = QSqlQuery(con_name)
    query.exec("SELECT notes FROM films")
    row_count = 0
    while query.next():
        row_count += 1
    query.finish()
    return row_count


def create_empty_table(con: QSqlDatabase) -> None:
    query = QSqlQuery(con)
    query.exec(CREATE_TABLE_SQLSTRING)
    query.finish()


def create_file(file_path: Path, contents: str) -> None:
    with open(file_path, "w") as f:
        f.write(contents)
    file_path.touch()


def get_file_contents(file_path: Path) -> Union[str, bytes]:
    try:
        with open(file_path, "r") as fp:
            return fp.read()
    except UnicodeDecodeError:
        with open(file_path, "rb") as bfp:
            return bfp.read()


def insert_sample_rows(con: QSqlDatabase, number_of_rows: int) -> None:
    query = QSqlQuery(con)
    query.prepare("INSERT INTO films DEFAULT VALUES")
    for i in range(number_of_rows):
        query.exec()
    query.finish()


def insert_records(model: QSqlTableModel, num: int) -> None:
    record = model.record(-1)
    for i in range(num):
        record.setValue(0, i)
        model.insertRecord(-1, record)


def open_virtual_con(con_name: str) -> QSqlDatabase:
    QSqlDatabase.addDatabase("QSQLITE", con_name)
    QSqlDatabase.database(con_name).setDatabaseName(":memory:")
    con = QSqlDatabase.database(con_name)
    return con


def return_false(*args: str, **kwargs: Dict[str, str]) -> bool:
    return False


def return_true(*args: str, **kwargs: Dict[str, str]) -> bool:
    return True
