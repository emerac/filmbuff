"""Unit tests for 'filmbuff.arguments'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from importlib.resources import path
import logging
from unittest import mock

import pytest

import filmbuff
from filmbuff import arguments


class TestCreateShortcut:
    @mock.patch("filmbuff.arguments.shutil.copy")
    @mock.patch("filmbuff.arguments.Path")
    def test(self, mock_path, mock_copy, tmp_path):
        mock_path.home.return_value = tmp_path
        with path("filmbuff.resources", "filmbuff.desktop") as resource_path:
            entry_src = resource_path
        entry_dest = (
            tmp_path
            / ".local"
            / "share"
            / "applications"
            / "filmbuff.desktop"
        )
        arguments.create_shortcut()
        mock_copy.assert_called_once_with(entry_src, entry_dest)


class TestParseArgs:
    @pytest.mark.parametrize(
        "args, expect",
        [
            pytest.param([], 0, id="no args"),
            pytest.param(["-v"], 1, id="-v"),
            pytest.param(["-vv"], 2, id="-vv"),
            pytest.param(["-vvv"], 3, id="-vvv"),
            pytest.param(["--verbose"], 1, id="--verbose"),
        ],
    )
    def test_no_error(self, args, expect):
        args = arguments.parse_args(args)
        assert args.verbose == expect

    def test_error(self):
        with pytest.raises(SystemExit):
            arguments.parse_args(["-q"])


class TestConfigureLogging:
    class MockLogger:
        def __init__(self):
            self.level = 0
            self.handlers = []

        def addHandler(self, handler):
            self.handlers.append(handler)

        def setLevel(self, level):
            self.level = level

    @pytest.mark.parametrize(
        "verbose, level",
        [
            pytest.param(0, 0, id="no settings"),
            pytest.param(1, 20, id="1 verbose -> INFO"),
            pytest.param(2, 10, id="2 verbose -> DEBUG"),
            pytest.param(3, 10, id="3 verbose -> DEBUG"),
        ],
    )
    def test(self, monkeypatch, verbose, level):
        def mock_null_handler(*args, **kwargs):
            return logging.NullHandler()

        def mock_stream_handler(*args, **kwargs):
            return logging.StreamHandler()

        mock_logger = TestConfigureLogging.MockLogger()

        monkeypatch.setattr(
            filmbuff.logger, "addHandler", mock_logger.addHandler,
        )
        monkeypatch.setattr(
            filmbuff.logger, "setLevel", mock_logger.setLevel,
        )
        monkeypatch.setattr(
            filmbuff, "null_handler", mock_null_handler,
        )
        monkeypatch.setattr(
            filmbuff, "stream_handler", mock_stream_handler,
        )

        assert mock_logger.level == 0
        assert mock_logger.handlers == []

        arguments.configure_logging(verbose)

        assert mock_logger.level == level
        if verbose == 0:
            assert mock_logger.handlers == [mock_null_handler]
        else:
            assert mock_logger.handlers == [mock_stream_handler]
