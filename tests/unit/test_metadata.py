"""Unit tests for 'filmbuff.metadata'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from unittest import mock

from PyQt5.QtCore import Qt
import pytest

from filmbuff import metadata


@pytest.mark.display
def test_empty_error_dialog(qtbot):
    dialog = metadata.EmptyErrorDialog("field")
    with qtbot.waitExposed(dialog):
        dialog.show()

    assert dialog.windowTitle() == "Search error"
    assert dialog.isVisible()
    qtbot.keyClick(dialog, Qt.Key_Return)
    assert not dialog.isVisible()


@pytest.mark.display
def test_unauthorized_error_dialog(qtbot):
    mock_HTTPError = mock.Mock(
        **{
            "response.status_code": "401",
            "response.reason": "reason",
            "response.json": lambda: {"status_message": "status message"},
        },
    )
    dialog = metadata.UnauthorizedErrorDialog(mock_HTTPError)
    with qtbot.waitExposed(dialog):
        dialog.show()

    assert dialog.windowTitle() == "Search error"
    assert "401" in dialog.text()
    assert dialog.isVisible()
    dialog.close()
    assert not dialog.isVisible()
