"""Unit tests for 'filmbuff.interface'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
from unittest import mock

from PyQt5.QtCore import (
    QVariant,
    Qt,
)
from PyQt5.QtSql import (
    QSqlDatabase,
    QSqlField,
    QSqlRecord,
)
from PyQt5.QtWidgets import (
    QFileDialog,
    QMessageBox,
)
import pytest

from filmbuff import interface
from tests import helpers


class TestInterface:
    @mock.patch("filmbuff.interface.QStandardPaths.writableLocation")
    @mock.patch("filmbuff.interface.QCoreApplication.applicationName")
    def test_make_data_path(self, mock_name, mock_location, tmp_path):
        mock_name.return_value = "test"
        mock_location.return_value = tmp_path
        assert interface.Interface.make_data_path() == (tmp_path / "test")
        assert (tmp_path / "test").exists()

    def test_update_record(self):
        old = QSqlRecord()
        new = QSqlRecord()

        for i in range(10):
            field = QSqlField(f"field {i}")
            old.append(field)
            new.append(field)
            old.setValue(f"field {i}", f"value {i}")
            new.setValue(f"field {i}", f"value {i}")
        field = QSqlField("ID", QVariant.Int)
        old.append(field)
        new.append(field)
        old.setValue("ID", 1)
        new.setValue("ID", 1)

        assert old == new
        new.setValue("field 5", "new value 5")
        assert old != new
        interface.Interface.update_record(old, new)
        assert old == new


@pytest.mark.display
def test_file_dialog(tmp_path, qtbot):
    options = {
        "path": str(tmp_path),
        "file_mode": QFileDialog.AnyFile,
        "accept_mode": QFileDialog.AcceptSave,
        "name_filter": "test (*.test)",
        "options": QFileDialog.DontConfirmOverwrite,
    }
    dialog = interface.FileDialog(**options)
    dialog.show()
    assert dialog.isVisible()

    assert dialog.parent() is None
    assert dialog.directory().path() == options["path"]
    assert dialog.fileMode() == options["file_mode"]
    assert dialog.acceptMode() == options["accept_mode"]
    assert len(dialog.nameFilters()) == 1
    assert dialog.nameFilters()[0] == options["name_filter"]
    assert dialog.options() == options["options"]

    dialog.close()
    assert not dialog.isVisible()


@pytest.mark.display
def test_message_dialog(qtbot):
    options = {
        "icon": QMessageBox.Icon.Information,
        "title": "title",
        "text": "text",
        "info": "info",
        "details": "details",
        "buttons": (QMessageBox.No | QMessageBox.Yes),
        "default": QMessageBox.No,
    }
    dialog = interface.MessageDialog(**options)

    qtbot.addWidget(dialog)
    dialog.show()
    assert dialog.isVisible()

    assert dialog.parent() is None
    assert dialog.windowTitle() == f"filmbuff - {options['title']}"
    assert dialog.text() == options["text"]
    assert dialog.informativeText() == options["info"]
    assert dialog.detailedText() == options["details"]
    assert dialog.standardButtons() == options["buttons"]
    assert dialog.defaultButton() == dialog.button(options["default"])

    qtbot.mouseClick(dialog.button(QMessageBox.No), Qt.LeftButton)
    dialog.close()
    assert not dialog.isVisible()


def test_copy_all_data(teardown_cons):
    con1_name = "virtual_1"
    con1 = helpers.open_virtual_con(con1_name)
    helpers.create_empty_table(con1)
    rows_inserted = 10
    helpers.insert_sample_rows(con1, rows_inserted)

    con2_name = "virtual_2"
    con2 = helpers.open_virtual_con(con2_name)
    helpers.create_empty_table(con2)

    con1_row_count = helpers.count_rows(con1)
    con2_row_count = helpers.count_rows(con2)
    assert con1_row_count == rows_inserted
    assert con2_row_count == 0

    interface.copy_all_data(con1_name, con2_name)

    con1_row_count = helpers.count_rows(con1)
    con2_row_count = helpers.count_rows(con2)
    assert con1_row_count == rows_inserted
    assert con2_row_count == rows_inserted


def test_create_empty_table(teardown_cons):
    con_name = "virtual"
    con = helpers.open_virtual_con(con_name)

    assert helpers.count_rows(con) == 0
    assert con.record("films").count() == 0

    interface.create_empty_table(con_name)

    assert helpers.count_rows(con) == 0
    assert con.record("films").count() == 28


def test_open_db_virtual(teardown_cons):
    con_name = "virtual"
    db_name = ":memory:"
    assert interface.open_db(con_name, db_name)
    assert len(QSqlDatabase.database().connectionNames()) == 1
    assert QSqlDatabase.database().connectionNames()[0] == con_name
    assert QSqlDatabase.database(con_name).isOpen()


def test_open_db_physical(tmp_path, teardown_cons):
    con_name = "physical"
    db_name = tmp_path / "test.sqlite"
    assert interface.open_db(con_name, str(db_name))
    assert len(QSqlDatabase.database().connectionNames()) == 1
    assert QSqlDatabase.database().connectionNames()[0] == con_name
    assert QSqlDatabase.database(con_name).isOpen()
    assert db_name.exists()


@mock.patch("filmbuff.interface.QSqlDatabase.open")
@mock.patch("filmbuff.interface.MessageDialog")
def test_open_db_fail(mock_dialog, mock_open, teardown_cons):
    mock_dialog(**{"exec": "None"})
    mock_open.return_value = False
    con_name = "virtual"
    db_name = ":memory:"
    assert not interface.open_db(con_name, db_name)
    mock_dialog.assert_called()


def test_select_all_data(teardown_cons):
    con_name = "virtual"
    con = helpers.open_virtual_con(con_name)
    helpers.create_empty_table(con)

    rows_inserted = 10
    helpers.insert_sample_rows(con, rows_inserted)

    query = interface.select_all_data(con_name)

    row_count = 0
    while query.next():
        row_count += 1
    query.finish()
    assert row_count == rows_inserted
