"""Unit tests for 'filmbuff.tablequery'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pytest

from filmbuff import tablequery


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param("sample text", "\"sample text\"", id="add"),
        pytest.param("\"sample text\"", "\"sample text\"", id="don't add"),
    ],
)
def test_add_quotes(query, result):
    assert tablequery.add_quotes(query) == result


def test_escape_query():
    query = "%_sample%_%text%_"
    result = "\\%\\_sample\\%\\_\\%text\\%\\_"
    assert tablequery.escape_query(query) == result


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param(
            "\"cast\":\"term\"",
            "\"cast one\":\"term\" or "
            "\"cast two\":\"term\" or "
            "\"cast three\":\"term\"",
            id="cast",
        ),
        pytest.param(
            "\"genre\":\"term\"",
            "\"genre one\":\"term\" or "
            "\"genre two\":\"term\" or "
            "\"genre three\":\"term\"",
            id="genre",
        ),
        pytest.param(
            "\"header 1\":\"term 1\" and "
            "\"cast\":\"term 2\" or "
            "\"header 3\":\"term 3\"",
            "\"header 1\":\"term 1\" and "
            "\"cast one\":\"term 2\" or "
            "\"cast two\":\"term 2\" or "
            "\"cast three\":\"term 2\" or "
            "\"header 3\":\"term 3\"",
            id="mixed",
        ),
    ],
)
def test_expand_query(query, result):
    assert tablequery.expand_query(query) == result


@pytest.mark.parametrize(
    "header_name, result",
    [
        pytest.param("header", "Header", id="non-id"),
        pytest.param("id header", "ID HEADER", id="id"),
    ],
)
def test_format_header_name(header_name, result):
    assert tablequery.format_header_name(header_name) == result


def test_get_special_operators():
    query = "\"text\" or \"text\" and \"text\" or \"text\""
    result = ["or", "and", "or"]
    assert tablequery.get_special_operators(query) == result


@pytest.mark.parametrize(
    "query, operators, result",
    [
        pytest.param(
            "\"header 1\":\"term 1\" or "
            "\"header 2\":\"term 2\" and "
            "\"header 3\":\"term 3\"",
            True,
            [
                "\"header 1\":\"term 1\"",
                "\"or\"",
                "\"header 2\":\"term 2\"",
                "\"and\"",
                "\"header 3\":\"term 3\"",
            ],
            id="operators",
        ),
        pytest.param(
            "\"header 1\":\"term 1\" or "
            "\"header 2\":\"term 2\" and "
            "\"header 3\":\"term 3\"",
            False,
            [
                "\"header 1\":\"term 1\"",
                "\"header 2\":\"term 2\"",
                "\"header 3\":\"term 3\"",
            ],
            id="no operators",
        ),
    ],
)
def test_get_subqueries(query, operators, result):
    assert tablequery.get_subqueries(query, operators) == result


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param(
            "term",
            "field 1 LIKE \"%term%\" ESCAPE \'\\\' OR "
            "field 2 LIKE \"%term%\" ESCAPE \'\\\' OR "
            "field 3 LIKE \"%term%\" ESCAPE \'\\\'",
            id="no escape",
        ),
        pytest.param(
            "%term_",
            "field 1 LIKE \"%\\%term\\_%\" ESCAPE \'\\\' OR "
            "field 2 LIKE \"%\\%term\\_%\" ESCAPE \'\\\' OR "
            "field 3 LIKE \"%\\%term\\_%\" ESCAPE \'\\\'",
            id="escape",
        ),
    ],
)
def test_handle_regular_query(query, headers, result):
    assert tablequery.handle_regular_query(query, headers) == result


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param(
            "\"Header 1\":\"term 1\" and "
            "\"Header 2\":\"term 2\" or "
            "\"Header 3\":\"term 3\"",
            "field 1 LIKE \"%term 1%\" ESCAPE \'\\\' AND "
            "field 2 LIKE \"%term 2%\" ESCAPE \'\\\' OR "
            "field 3 LIKE \"%term 3%\" ESCAPE \'\\\'",
            id="no escape",
        ),
        pytest.param(
            "\"Header 1\":\"%term 1_\" and "
            "\"Header 2\":\"%term 2_\" and "
            "\"Header 3\":\"%term 3_\"",
            "field 1 LIKE \"%\\%term 1\\_%\" ESCAPE \'\\\' AND "
            "field 2 LIKE \"%\\%term 2\\_%\" ESCAPE \'\\\' AND "
            "field 3 LIKE \"%\\%term 3\\_%\" ESCAPE \'\\\'",
            id="escape",
        ),
    ],
)
def test_handle_special_query(query, headers, result):
    assert tablequery.handle_special_query(query, headers) == result


def test_handle_special_query_error(monkeypatch, headers):
    def return_false(*args, **kwargs):
        return False
    monkeypatch.setattr(tablequery, "is_valid_special_query", return_false)
    query = "\"header\":\"term\""
    with pytest.raises(ValueError):
        tablequery.handle_special_query(query, headers)


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param("\"cast\":\"term\"", True, id="cast"),
        pytest.param("\"genre\":\"term\"", True, id="genre"),
        pytest.param(
            "\"genre\":\"term 1\" and \"cast\":\"term 2\"",
            True,
            id="mixed",
        ),
        pytest.param("\"genre\"\"term\"", False, id="wrong syntax"),
    ],
)
def test_is_expanding_query(query, result):
    assert tablequery.is_expanding_query(query) == result


@pytest.mark.parametrize(
    "query, result",
    [
        pytest.param("\"Header 1\":\"term\"", True, id="non-expanding"),
        pytest.param("\"cast\":\"term\"", True, id="expanding cast"),
        pytest.param("\"genre\":\"term\"", True, id="expanding genre"),
        pytest.param("\"Not Header 1\":\"term\"", False, id="wrong header"),
        pytest.param("query", False, id="regular"),
    ],
)
def test_is_special_query(query, headers, result):
    assert tablequery.is_special_query(query, headers) == result


@pytest.mark.parametrize(
    "subqueries, operators, result",
    [
        pytest.param(
            [
                "\"Header 1\":\"term 1\"",
                "\"Header 2\":\"term 2\"",
                "\"Header 3\":\"term 3\"",
            ],
            ["and", "or"],
            True,
            id="valid",
        ),
        pytest.param(
            [
                "\"Not Header 1\":\"term 1\"",
            ],
            [],
            False,
            id="wrong header",
        ),
        pytest.param(
            [
                "\"Header 1\":\"term 1\"",
                "\"Header 2\":\"term 2\"",
            ],
            ["not"],
            False,
            id="wrong operator",
        ),
        pytest.param(
            [
                "\"Header 1\"\"term 1\"",
            ],
            [],
            False,
            id="wrong syntax",
        ),
    ],
)
def test_is_valid_special_query(subqueries, operators, headers, result):
    assert tablequery.is_valid_special_query(
        subqueries,
        operators,
        headers,
    ) == result


@pytest.mark.parametrize(
    "text, result",
    [
        pytest.param("\"sample text\"", "sample text", id="quotes"),
        pytest.param("sample text", "sample text", id="no quotes"),
        pytest.param("\"\"", "\"\"", id="only quotes"),
    ],
)
def test_remove_quotes(text, result):
    assert tablequery.remove_quotes(text) == result
