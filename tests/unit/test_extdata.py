"""Unit tests for 'filmbuff.extdata'.

Copyright (C) 2021 emerac

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
import pytest
import requests

from filmbuff import extdata


class MockResponse:
    def __init__(self, status_code):
        self.status_code = status_code

    def json(self):
        return {"mock_key": "mock_value"}

    def raise_for_status(self):
        if self.status_code == 200:
            return None
        else:
            raise requests.exceptions.HTTPError


class TestGetCast:
    def test(self, sample_data):
        cast = extdata.get_cast(sample_data)
        assert len(cast) == 3
        assert cast[0] == "Full Name 1"
        assert cast[1] == "Full Name 2"
        assert cast[2] == "Full Name 3"


class TestGetDirector:
    def test_director(self, sample_data):
        director = extdata.get_director(sample_data)
        assert director == "Full Name 4"

    def test_no_director(self, sample_data):
        del sample_data["credits"]["crew"][0]
        director = extdata.get_director(sample_data)
        assert director is None


class TestGetGenres:
    def test(self, sample_data):
        genres = extdata.get_genres(sample_data)
        assert len(genres) == 3
        assert genres[0] == "Genre 1"
        assert genres[1] == "Genre 2"
        assert genres[2] == "Genre 3"


class TestGetFilmInfo:
    def test_no_error(self, monkeypatch):
        def get_mock_response(*args, **kwargs):
            return MockResponse(200)

        monkeypatch.setattr(requests, "get", get_mock_response)

        try:
            info = extdata.get_film_info("key", "film_id")
        except requests.exceptions.HTTPError:
            pytest.fail()

        assert "mock_key" in info.keys()
        assert "mock_value" in info.values()

    def test_error(self, monkeypatch):
        def get_mock_response(*args, **kwargs):
            return MockResponse(404)

        monkeypatch.setattr(requests, "get", get_mock_response)

        with pytest.raises(requests.exceptions.RequestException):
            extdata.get_film_info("key", "film_id")


class TestGetSearchResults:
    @pytest.mark.parametrize(
        "args",
        [
            pytest.param(("key", "query", "", 1), id="empty year"),
            pytest.param(("key", "query", "1000", 1), id="set year"),
        ],
    )
    def test_no_error(self, monkeypatch, args):
        def get_mock_response(*args, **kwargs):
            return MockResponse(200)

        monkeypatch.setattr(requests, "get", get_mock_response)

        try:
            info = extdata.get_search_results(*args)
        except requests.exceptions.HTTPError:
            pytest.fail()

        assert "mock_key" in info.keys()
        assert "mock_value" in info.values()

    @pytest.mark.parametrize(
        "args",
        [
            pytest.param(("key", "query", "", 1), id="empty year"),
            pytest.param(("key", "query", "1000", 1), id="set year"),
        ],
    )
    def test_error(self, monkeypatch, args):
        def get_mock_response(*args, **kwargs):
            return MockResponse(404)

        monkeypatch.setattr(requests, "get", get_mock_response)

        with pytest.raises(requests.exceptions.RequestException):
            extdata.get_search_results(*args)
